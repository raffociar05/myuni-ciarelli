package com.example.myuni;

public class Libretto {

    private String materia;
    private String voto;
    private String cfu;
    private String dataEsame;

    public Libretto(){}

    public Libretto(String materia,String voto, String cfu,String dataEsame) {
        this.materia = materia;
        this.voto = voto;
        this.cfu = cfu;
        this.dataEsame = dataEsame;
    }

    public String getMateria() {
        return materia;
    }

    public String getVoto() {
        return voto;
    }

    public String getCfu() { return cfu; }

    public String getDataEsame() { return dataEsame; }
}
