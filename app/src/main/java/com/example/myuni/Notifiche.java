package com.example.myuni;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class Notifiche extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifiche);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        final  TextView link = findViewById(R.id.link);
        final TextView intro = findViewById(R.id.intro);
        final TextView benvenuto = findViewById(R.id.benvenuto);
        intro.setText(R.string.linkUni);

        final Button login = findViewById(R.id.buttonLogin);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FALLITO", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        // Log and toast
                        Log.e("TOKEN: ", token);
                        Toast.makeText(Notifiche.this, "TOKEN: "+token, Toast.LENGTH_SHORT).show();

                    }
                });

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){

            link.setText(bundle.getString("body"));
            benvenuto.setText(bundle.getString("title"));

            for(String key : bundle.keySet()){
                Object value = bundle.get(key);
                Log.e("NOTIFICA","Key: "+key + " Value: "+value);

            }
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin = new Intent(Notifiche.this,MainActivity.class);
                startActivity(intentLogin);
            }
        });



    }


}
