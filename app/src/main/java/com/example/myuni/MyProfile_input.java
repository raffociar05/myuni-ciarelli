package com.example.myuni;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class MyProfile_input extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText nome, cognome, sede, matricola, den_corso;
    Button inviaDati;
    Spinner spinnerDip;
    RadioGroup radioGroup;
    RadioButton radioButton;
    TextView benvenutoRegistrati,tipoLaurea,dipartimento;
    ConstraintLayout layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_input);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setTitle(R.string.registradati);

        //freccia
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        final Map<String,Object> dati= new HashMap<>();

        final FirebaseAuth auth = FirebaseAuth.getInstance();

        final FirebaseUser user = auth.getCurrentUser();

        layout = findViewById(R.id.myProfileInput);
        layout.setBackgroundColor(Color.parseColor("#FFFFCC"));

        nome = findViewById(R.id.nomeEt);
        cognome = findViewById(R.id.cognomeEt);
        sede = findViewById(R.id.sedeUniEt);
        matricola = findViewById(R.id.matricolaEt);
        den_corso = findViewById(R.id.denCorsoEt);
        benvenutoRegistrati=findViewById(R.id.benvenutoRegistrati);
        tipoLaurea = findViewById(R.id.tipoLaureaIn);
        dipartimento = findViewById(R.id.dipartimentoIn);
        inviaDati = findViewById(R.id.inviaDati);

        benvenutoRegistrati.setText(R.string.benvenutoReg);
        tipoLaurea.setText(R.string.tipolaureaIn);
        dipartimento.setText(R.string.dipartimentoIn);
        inviaDati.setText(R.string.confermabut);

        nome.setHint(R.string.nomereg);
        cognome.setHint(R.string.cognomereg);
        sede.setHint(R.string.sedereg);
        matricola.setHint(R.string.matricolareg);
        den_corso.setHint(R.string.dencorsoreg);

        inviaDati = findViewById(R.id.inviaDati);

        radioGroup = findViewById(R.id.radioGroup);

        spinnerDip = findViewById(R.id.spinnerDip);

        ArrayAdapter<CharSequence> myAdapter =  ArrayAdapter.createFromResource(this,
                R.array.dipartimento,android.R.layout.simple_spinner_item);

        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDip.setAdapter(myAdapter);
        spinnerDip.setOnItemSelectedListener(this);


            inviaDati.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    boolean error=false;

                    if (nome.getText().toString().isEmpty()) {
                        error = true;
                        nome.setText("");
                        nome.setHint(R.string.errorNome);
                        nome.setHintTextColor(Color.RED);
                    }
                    if (cognome.getText().toString().isEmpty()) {
                        error = true;
                        cognome.setText("");
                        cognome.setHint(R.string.errorCognome);
                        cognome.setHintTextColor(Color.RED);
                    }
                    if (sede.getText().toString().isEmpty()) {
                        error = true;
                        sede.setText("");
                        sede.setHint(R.string.errorSede);
                        sede.setHintTextColor(Color.RED);
                    }
                    if (!matricola.getText().toString().isEmpty()) {
                        if (Integer.parseInt(matricola.getText().toString()) < 3000000
                                || Integer.parseInt(matricola.getText().toString()) > 4000000
                        ) {
                            error = true;
                            matricola.setText("");
                            matricola.setHint(R.string.errorMatricola);
                            matricola.setHintTextColor(Color.RED);
                            }
                        }else {
                            error = true;
                            matricola.setText("");
                            matricola.setHint(R.string.errorMatricolaVuota);
                            matricola.setHintTextColor(Color.RED);
                        }

                        if (den_corso.getText().toString().isEmpty()) {
                            error = true;
                            den_corso.setText("");
                            den_corso.setHint(R.string.errorLaurea);
                            den_corso.setHintTextColor(Color.RED);
                        }


                    if (!error) {

                        dati.put("nome", nome.getText().toString());
                        dati.put("cognome", cognome.getText().toString());
                        dati.put("sedeUni", sede.getText().toString());
                        dati.put("matricola", matricola.getText().toString());


                        int radioId = radioGroup.getCheckedRadioButtonId();
                        radioButton = findViewById(radioId);
                        dati.put("tipoLaurea", radioButton.getText().toString());

                        dati.put("dipartimento", spinnerDip.getSelectedItem().toString());

                        dati.put("denominazioneCorso", den_corso.getText().toString());

                        db.collection("users")
                                .document(user.getUid()).set(dati)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        Toast.makeText(MyProfile_input.this, R.string.datiInseriti, Toast.LENGTH_LONG).show();

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Toast.makeText(MyProfile_input.this, R.string.erroreVerificato, Toast.LENGTH_LONG).show();

                                    }
                                });


                        Intent intentProfile = new Intent(MyProfile_input.this, MyProfile.class);
                        startActivity(intentProfile);

                    }
                }


            });


        }


    //RADIO GROUP LAUREA
    public void select_laurea(View v){
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);

       // Toast.makeText(this,"Hai selezionato: Laurea "+radioButton.getText(),Toast.LENGTH_SHORT).show();

    }

    //MENU' A TENDINA DIPARTIMENTO
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        /*String text = "Hai selezionato: "+parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(),text,Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }


}
