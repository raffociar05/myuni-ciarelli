package com.example.myuni;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    final String TOKEN = "TOKEN";
    final String DATI_MESSAGGIO = "DATI_MESSAGGIO";
    final String MITTENTE_MESSAGGIO = "MITTENTE_MESSAGGIO";
    final String CORPO_MESSAGGIO = "CORPO_MESSAGGIO";


    @Override
    public void onNewToken(String token) {
        Log.d(TOKEN, "Refreshed token: " + token);

        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(String token) {
        //TODO: il metodo dovrebbe essere creato

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(MITTENTE_MESSAGGIO, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(DATI_MESSAGGIO, "Message data payload: " + remoteMessage.getData());

        }

        if (remoteMessage.getNotification() != null) {
            Log.d(CORPO_MESSAGGIO, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            inviaNotifica(remoteMessage);

        }

    }

    public void inviaNotifica(RemoteMessage remoteMessage){

        Intent intent = new Intent(this,Notifiche.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra("title",remoteMessage.getData().get("title"));
        intent.putExtra("body",remoteMessage.getData().get("body"));

        PendingIntent pendingIntent =
                PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);

        String channelId = "notifiche";

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,channelId);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(remoteMessage.getNotification().getTitle());
        builder.setContentText(remoteMessage.getNotification().getBody());

        builder.setExtras(intent.getExtras());

        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);


        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel
                    = new NotificationChannel(channelId,"Default Channel",NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
        }
        else {
            manager.notify(0, builder.build());
        }
    }

}
