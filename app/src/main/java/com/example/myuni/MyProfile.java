package com.example.myuni;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class MyProfile extends AppCompatActivity {

    TextView nome, cognome, matricola, tipoLaurea, sede, dipartimento, corso, libretto, ilmioprofilo;
    Button modifica, elimina, logout;
    ConstraintLayout myProfile;
    ImageView librettoImage;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        setTitle(R.string.ilmioprofilo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        context = this;

        myProfile = findViewById(R.id.myProfile);
        myProfile.setBackgroundColor(Color.parseColor("#FFFFCC"));

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        final FirebaseAuth firebaseAuthM = FirebaseAuth.getInstance();
        final FirebaseUser user = firebaseAuthM.getCurrentUser();

        nome = findViewById(R.id.nome);
        cognome = findViewById(R.id.cognome);
        matricola = findViewById(R.id.matricola);
        tipoLaurea = findViewById(R.id.tipoLaurea);
        sede = findViewById(R.id.sede);
        dipartimento = findViewById(R.id.dipartimento);
        corso = findViewById(R.id.corso);
        libretto = findViewById(R.id.librettoTv);
        ilmioprofilo = findViewById(R.id.ilmioprofilo);

        ilmioprofilo.setText(R.string.ilmioprofilo);
        nome.setText("");
        cognome.setText("");
        matricola.setText("");
        tipoLaurea.setText("");
        sede.setText("");
        dipartimento.setText("");
        corso.setText("");

        modifica = findViewById(R.id.modificaProfilo);
        elimina = findViewById(R.id.eliminaProfilo);
        modifica.setText(R.string.modificaProfilo);
        elimina.setText(R.string.eliminaProfilo);

        libretto.setText(R.string.ilmiolibretto);
        libretto.setPaintFlags(libretto.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        modifica = findViewById(R.id.modificaProfilo);
        elimina = findViewById(R.id.eliminaProfilo);
        logout = findViewById(R.id.logout);

        librettoImage = findViewById(R.id.librettoImage);


        db.collection("users").document(user.getUid())
                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                nome.setText(documentSnapshot.getString("nome"));
                cognome.setText(documentSnapshot.getString("cognome"));
                matricola.setText(getString(R.string.matricola) + " " + documentSnapshot.getString("matricola"));
                tipoLaurea.setText(getString(R.string.laurea) + " " + documentSnapshot.getString("tipoLaurea"));
                sede.setText(getString(R.string.sede) + " " + documentSnapshot.getString("sedeUni"));
                dipartimento.setText(getString(R.string.dipartimento) + " " + documentSnapshot.getString("dipartimento"));
                corso.setText(getString(R.string.corso) + " " + documentSnapshot.getString("denominazioneCorso"));

            }

        });

        modifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentModifica = new Intent(MyProfile.this, ModificaProfilo.class);
                startActivity(intentModifica);

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intentLogout = new Intent(MyProfile.this, MainActivity.class);
                startActivity(intentLogout);

                Toast.makeText(MyProfile.this, R.string.logout, Toast.LENGTH_LONG).show();

            }
        });


        elimina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context)
                        .setTitle(R.string.eliminaaccount)
                        .setMessage(R.string.sicuro)
                        .setPositiveButton(R.string.conferma, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(MyProfile.this, R.string.accountrimosso, Toast.LENGTH_LONG).show();
                                            Intent intentMain = new Intent(MyProfile.this, MainActivity.class);
                                            startActivity(intentMain);
                                        } else {
                                            Toast.makeText(MyProfile.this, R.string.errRimozioneAccount, Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });


                                db.collection("users").document(user.getUid()).delete();

                            }
                        });

                dialog.setNegativeButton(R.string.annulla, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        librettoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLibretto = new Intent(MyProfile.this, IlMioLibretto.class);
                startActivity(intentLibretto);
            }
        });


    }



}

