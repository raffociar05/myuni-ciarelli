package com.example.myuni;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class IlMioLibretto extends AppCompatActivity {



    FloatingActionButton fab;
    CoordinatorLayout ilMioLayout;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference collectionReference = db.collection("votes");

    final Map<String,Object> dati= new HashMap<>();
    final FirebaseAuth auth = FirebaseAuth.getInstance();
    final FirebaseUser user = auth.getCurrentUser();

    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_il_mio_libretto);
        setUpRecyclerView();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setTitle(R.string.ilmiolibretto);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ilMioLayout = findViewById(R.id.ilMioLibrettoLayout);
        ilMioLayout.setBackgroundColor(Color.parseColor("#FFFFCC"));

        fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.dialog_libretto);
                dialog.setCancelable(false);

                final EditText materia=dialog.findViewById(R.id.materiaEt);
                final EditText voto=dialog.findViewById(R.id.votoEt);
                final EditText cfu = dialog.findViewById(R.id.cfuEt);
                final TextView data = dialog.findViewById(R.id.dataEt);

                final Button annulla=dialog.findViewById(R.id.annulla);
                final Button conferma=dialog.findViewById(R.id.conferma);
                final CheckBox lode = dialog.findViewById(R.id.checkLode);


                data.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar newCalendar = Calendar.getInstance();
                        DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, month, dayOfMonth);
                                month+=1;
                                String date =  dayOfMonth + "/"+ month + "/" +year;
                                data.setText(date);
                            }
                        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.show();
                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    }
                });


                conferma.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {

                        boolean error = false;



                        if (materia.getText().toString().isEmpty()) {
                            error = true;
                            materia.setText("");
                            materia.setHint(R.string.erroreMateria);
                            materia.setHintTextColor(Color.RED);
                        }

                        if (!voto.getText().toString().isEmpty()) {
                            if (Integer.parseInt(voto.getText().toString()) < 18
                                    || Integer.parseInt(voto.getText().toString()) > 30) {
                                error = true;
                                voto.setText("");
                                voto.setHint(R.string.erroreVoto);
                                voto.setHintTextColor(Color.RED);
                            }
                        } else {
                            error = true;
                            voto.setText("");
                            voto.setHint(R.string.erroreVotoVuoto);
                            voto.setHintTextColor(Color.RED);
                        }

                        if(lode.isChecked() &&
                                Integer.parseInt(voto.getText().toString()) !=30){
                            error = true;
                            voto.setText("");
                            voto.setHint(R.string.erroreLode);
                            voto.setHintTextColor(Color.RED);
                        }

                        if(data.getText().toString().isEmpty()){

                            error = true;
                            data.setText("");
                            data.setHint(R.string.errorData);
                            data.setHintTextColor(Color.RED);
                        }


                        if(!cfu.getText().toString().isEmpty()){
                            if(Integer.parseInt(cfu.getText().toString()) < 4
                                || Integer.parseInt(cfu.getText().toString()) > 15){
                                error=true;
                                cfu.setText("");
                                cfu.setHint(R.string.errorCfu);
                                cfu.setHintTextColor(Color.RED);
                            }
                        }
                        else{
                            error=true;
                            cfu.setText("");
                            cfu.setHint(R.string.errorCfuVuoto);
                            cfu.setHintTextColor(Color.RED);
                        }


                        if (!error) {

                            dati.put("materia", materia.getText().toString());
                            if(lode.isChecked() &&
                                    Integer.parseInt(voto.getText().toString()) == 30){
                                dati.put("voto", "30L");
                            } else {
                                dati.put("voto", voto.getText().toString());
                            }
                            dati.put("cfu",cfu.getText().toString());
                            dati.put("userId",user.getUid());
                            dati.put("dataEsame",data.getText().toString());


                            db.collection("votes")
                                    .document().set(dati)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(IlMioLibretto.this, R.string.esameAggiunto, Toast.LENGTH_LONG).show();
                                            dialog.hide();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(IlMioLibretto.this, R.string.verificatoErrore, Toast.LENGTH_LONG).show();
                                            dialog.hide();
                                        }
                                    });
                            dialog.hide();
                        }

                    }
                });

                annulla.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });
                dialog.show();
            }
        });


    }

    private void setUpRecyclerView(){

        Query query = collectionReference.whereEqualTo("userId",user.getUid());

        FirestoreRecyclerOptions<Libretto> options = new FirestoreRecyclerOptions.Builder<Libretto>()
                .setQuery(query,Libretto.class)
                .build();

        adapter  = new MyAdapter(options);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    adapter.deleteItem(viewHolder.getAdapterPosition());
                    Toast.makeText(IlMioLibretto.this, R.string.esameRimosso, Toast.LENGTH_LONG).show();
            }
        }).attachToRecyclerView(recyclerView);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MyProfile.class);
        startActivityForResult(myIntent, 0);
        return true;
    }




}
