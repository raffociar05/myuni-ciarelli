package com.example.myuni;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    EditText email;
    EditText password;
    Button inviaLog;
    TextView registrati;
    FirebaseAuth mAuth;
    ConstraintLayout mainLayout;
    TextView pswDimenticata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setTitle("Home");

        mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setBackgroundColor(Color.parseColor("#FFFFCC"));

        email = findViewById(R.id.emailLogin);
        password = findViewById(R.id.passwordLogin);
        registrati = findViewById(R.id.registrati);
        TextView myUni = findViewById(R.id.welcome);
        pswDimenticata = findViewById(R.id.pswDimenticata);
        myUni.setText(getString(R.string.welcome)+"\nmyUni");

        registrati.setText(R.string.registrati);

        inviaLog = findViewById(R.id.inviaLogin);

        mAuth = FirebaseAuth.getInstance();

        inviaLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!email.getText().toString().isEmpty()) {
                    if (!password.getText().toString().isEmpty()) {
                        loginUsers();
                    } else {
                        password.setHintTextColor(Color.RED);
                        password.setHint(R.string.errorPswVuota);
                    }
                }
                else{
                    email.setHintTextColor(Color.RED);
                    email.setHint(R.string.errorEmailVuota);
                }
            }
        });


        registrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegistrati = new Intent(MainActivity.this,Registrazione.class);
                startActivity(intentRegistrati);
        }
        });




        pswDimenticata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.dialog_recupera_psw);
                dialog.setCancelable(false);
                dialog.show();

                final EditText inserisciEmail = dialog.findViewById(R.id.emailRecupero);
                final Button inviaEmail = dialog.findViewById(R.id.inviaEmailButton);
                final Button annullaInvia = dialog.findViewById(R.id.annullaInvia);

                inviaEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mAuth.sendPasswordResetEmail(inserisciEmail.getText().toString())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Toast.makeText(getApplicationContext(),R.string.emailInviata,Toast.LENGTH_LONG).show();
                                            dialog.hide();
                                        }
                                        else{
                                            Toast.makeText(getApplicationContext(),R.string.verificatoErrore,Toast.LENGTH_LONG).show();
                                            dialog.hide();
                                        }
                                    }
                                });

                    }
                });

                annullaInvia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });


            }

        });


    }

    private void loginUsers(){

        String emailS, passwordS;
        emailS = email.getText().toString();
        passwordS = password.getText().toString();

        mAuth.signInWithEmailAndPassword(emailS,passwordS)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(getApplicationContext(),R.string.loginok,Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(MainActivity.this, MyProfile.class);
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(getApplicationContext(),R.string.loginFallita,Toast.LENGTH_LONG).show();
                        }

                    }
                });

        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        if(item.getItemId()==R.id.action_info) {
            Toast.makeText(this, R.string.infoApp, Toast.LENGTH_SHORT).show();
        }

        final String[] addresses = new String[1];
        addresses[0] = "raffaello.ciarelli@studenti.unich.it";

        if (item.getItemId()==R.id.action_email){
            composeEmail(addresses, getString(R.string.segnalaEmail));
        }

        return super.onOptionsItemSelected(item);
    }

    public void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


}
