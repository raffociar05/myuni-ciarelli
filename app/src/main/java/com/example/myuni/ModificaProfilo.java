package com.example.myuni;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class ModificaProfilo extends AppCompatActivity {

    TextView modNome, modCognome, modSede, modMatricola, modCorso;
    Button buttonModifica;
    Spinner modSpinner;
    RadioGroup modRadioGroup;
    RadioButton modRadioButton;
    ConstraintLayout modLayout;
    TextView introMod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_profilo);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        modLayout = findViewById(R.id.modLayout);
        modLayout.setBackgroundColor(Color.parseColor("#FFFFCC"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        introMod = findViewById(R.id.introModifica);
        introMod.setText(R.string.modificadati);

        modNome = findViewById(R.id.modNome);
        modCognome = findViewById(R.id.modCognome);
        modSede = findViewById(R.id.modSede);
        modMatricola = findViewById(R.id.modMatricola);
        modCorso = findViewById(R.id.modCorso);
        buttonModifica = findViewById(R.id.modButton);
        modSpinner = findViewById(R.id.modSpinner);
        modRadioGroup = findViewById(R.id.modRadioGroup);
        buttonModifica = findViewById(R.id.modButton);
        modSpinner = findViewById(R.id.modSpinner);


        final Map<String,Object> dati= new HashMap<>();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        final FirebaseAuth firebaseAuthM = FirebaseAuth.getInstance();
        final FirebaseUser user = firebaseAuthM.getCurrentUser();

        ArrayAdapter<CharSequence> myAdapter =  ArrayAdapter.createFromResource(this,
                R.array.dipartimento,android.R.layout.simple_spinner_item);

        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modSpinner.setAdapter(myAdapter);

        db.collection("users").document(user.getUid())
                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                modNome.setText(documentSnapshot.getString("nome"));
                modCognome.setText(documentSnapshot.getString("cognome"));
                modMatricola.setText(documentSnapshot.getString("matricola"));

                modSede.setText(documentSnapshot.getString("sedeUni"));
                modSpinner.setSelection(getSpinner(modSpinner, documentSnapshot.getString("dipartimento")));
                modCorso.setText(documentSnapshot.getString("denominazioneCorso"));

            }

        });


        buttonModifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean error=false;

                if (modNome.getText().toString().isEmpty()) {
                    error = true;
                    modNome.setText("");
                    modNome.setHint(R.string.errorNome);
                    modNome.setHintTextColor(Color.RED);
                }
                if (modCognome.getText().toString().isEmpty()) {
                    error = true;
                    modCognome.setText("");
                    modCognome.setHint(R.string.errorCognome);
                    modCognome.setHintTextColor(Color.RED);
                }
                if (modSede.getText().toString().isEmpty()) {
                    error = true;
                    modSede.setText("");
                    modSede.setHint(R.string.errorSede);
                    modSede.setHintTextColor(Color.RED);
                }
                if (!modMatricola.getText().toString().isEmpty()) {
                    if (Integer.parseInt(modMatricola.getText().toString()) < 3000000
                            || Integer.parseInt(modMatricola.getText().toString()) > 4000000
                    ) {
                        error = true;
                        modMatricola.setText("");
                        modMatricola.setHint(R.string.errorMatricola);
                        modMatricola.setHintTextColor(Color.RED);
                    }
                }else {
                    error = true;
                    modMatricola.setText("");
                    modMatricola.setHint(R.string.errorMatricolaVuota);
                    modMatricola.setHintTextColor(Color.RED);
                }

                if (modCorso.getText().toString().isEmpty()) {
                    error = true;
                    modCorso.setText("");
                    modCorso.setHint(R.string.errorLaurea);
                    modCorso.setHintTextColor(Color.RED);
                }


                if (!error) {

                    dati.put("nome", modNome.getText().toString());
                    dati.put("cognome", modCognome.getText().toString());
                    dati.put("sedeUni", modSede.getText().toString());
                    dati.put("matricola", modMatricola.getText().toString());
                    dati.put("idUser",user.getUid());
                    dati.put("denominazioneCorso", modCorso.getText().toString());

                    int radioId = modRadioGroup.getCheckedRadioButtonId();
                    modRadioButton = findViewById(radioId);
                    dati.put("tipoLaurea", modRadioButton.getText().toString());

                    dati.put("dipartimento", modSpinner.getSelectedItem().toString());



                    db.collection("users")
                            .document(user.getUid()).set(dati)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    Toast.makeText(ModificaProfilo.this, R.string.modOk, Toast.LENGTH_LONG).show();

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                    Toast.makeText(ModificaProfilo.this, R.string.erroreVerificato, Toast.LENGTH_LONG).show();

                                }
                            });


                    Intent intentProfile = new Intent(ModificaProfilo.this, MyProfile.class);
                    startActivity(intentProfile);

                }
            }


        });


    }


    public void select_laurea(View v){
        int radioId = modRadioGroup.getCheckedRadioButtonId();
        modRadioButton = findViewById(radioId);


    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MyProfile.class);
        startActivityForResult(myIntent, 0);


        Toast.makeText(ModificaProfilo.this, R.string.modFallita, Toast.LENGTH_LONG).show();

        return true;

    }

    public int getSpinner(Spinner spinner, String string) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(string)) {
                return i;
            }
        }
        return 0;
    }



}