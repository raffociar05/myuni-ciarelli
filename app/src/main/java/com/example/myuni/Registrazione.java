package com.example.myuni;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Registrazione extends AppCompatActivity {

    TextView registraAccount;
    EditText email;
    EditText password;
    EditText confermaPassword;
    Button registrati;
    FirebaseAuth mAuth;
    ConstraintLayout registrazioneLayout;
    Switch switchNotifiche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrazione);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setTitle(R.string.registra);

        //freccia
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        registrazioneLayout = findViewById(R.id.registrazioneLayout);
        registrazioneLayout.setBackgroundColor(Color.parseColor("#FFFFCC"));

        registraAccount = findViewById(R.id.benvenutoRegistrati);
        email = findViewById(R.id.emailEt);
        password = findViewById(R.id.passwordEt);
        confermaPassword = findViewById(R.id.confermaPasswordEt);
        registrati = findViewById(R.id.inviaRegister);
        switchNotifiche = findViewById(R.id.switch1);

        registraAccount.setText(R.string.registraaccount);

        confermaPassword.setHint(R.string.confermapassword);

        mAuth = FirebaseAuth.getInstance();

        registrati.setText(R.string.registratii);
        registrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerNewUser();
            }
        });

        switchNotifiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switchNotifiche.isChecked()){
                    FirebaseMessaging.getInstance().subscribeToTopic("notifiche")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(),R.string.msg_subscribed,
                                                Toast.LENGTH_LONG).show();
                                    }

                                }
                            });
                }
            }
        });


    }

    private void registerNewUser(){

        String emailS, passwordS, confPasswordS;
        emailS = email.getText().toString();
        passwordS = password.getText().toString();
        confPasswordS = confermaPassword.getText().toString();
        boolean error = false;

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        if(emailS.isEmpty() || emailS.length()<5 || !emailValidator(emailS)) {
            error=true;
            email.setText("");
            email.setHintTextColor(Color.RED);
            email.setHint(R.string.errorEmail);
            }

        if(!passwordS.isEmpty()){
            if(passwordS.length()<8){
                error = true;
                password.setText("");
                password.setHintTextColor(Color.RED);
                password.setHint(R.string.errorPswCorta);
            }

        }
        else {
            error = true;
            password.setText("");
            password.setHintTextColor(Color.RED);
            password.setHint(R.string.errorPswVuota);
        }


        if(!passwordS.equals(confPasswordS)){
            error=true;
            password.setText("");
            confermaPassword.setText("");
            password.setHintTextColor(Color.RED);
            confermaPassword.setHintTextColor(Color.RED);
            password.setHint(R.string.errorConfPsw);
            confermaPassword.setHint(R.string.errorConfPsw);

        }


        if(!error){

            mAuth.createUserWithEmailAndPassword(emailS,passwordS)
            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(getApplicationContext(),R.string.registrazioneok,Toast.LENGTH_LONG).show();
                        Intent intentLogin = new Intent(Registrazione.this,MyProfile_input.class);
                        startActivity(intentLogin);
                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.registrazionefallita,
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else{
            alertDialog.setTitle(R.string.erroreDialogTitolo);
            alertDialog.setMessage(String.valueOf(R.string.erroreDialogMess));
        }


    }

    public static boolean emailValidator(String input){

        String emailRegex ="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern emailPat = Pattern.compile(emailRegex,Pattern.CASE_INSENSITIVE);
        Matcher matcher = emailPat.matcher(input);
        return matcher.find();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

}
