package com.example.myuni;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class MyAdapter extends FirestoreRecyclerAdapter<Libretto, MyAdapter.viewHolder> {


    public MyAdapter(@NonNull FirestoreRecyclerOptions<Libretto> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull viewHolder holder, int position, @NonNull Libretto libretto) {

        holder.textViewMateria.setText(libretto.getMateria());
        holder.textViewVoto.setText(libretto.getVoto());
        holder.textViewCfu.setText("Cfu: "+libretto.getCfu());
        holder.textViewData.setText("Data: "+libretto.getDataEsame());

        Log.e("ERROR", libretto.getDataEsame());

    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.libretto_card,parent,false);

        return new viewHolder(v);
    }

    public void deleteItem(int position){

        getSnapshots().getSnapshot(position).getReference().delete();

    }

    class viewHolder extends RecyclerView.ViewHolder {

        TextView textViewMateria, textViewVoto, textViewCfu, textViewData;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            textViewMateria = itemView.findViewById(R.id.materia);
            textViewVoto = itemView.findViewById(R.id.voto);
            textViewCfu = itemView.findViewById(R.id.cfu);
            textViewData = itemView.findViewById(R.id.dataS);


        }
    }

}
